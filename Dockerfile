FROM amd64/centos:latest as base

RUN yum update -y && \
    yum install -y which && \
    yum install -y python3.8 \
    python3-pip && \
    rm -rf /var/lib/apt/lists/*

ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

WORKDIR /checker

ADD Pipfile*  /checker/

RUN export PIPENV_USE_SYSTEM=1

RUN pip3 install --upgrade pipenv && \
    #pipenv lock --clear && \
    #pipenv lock && \
    pipenv install --ignore-pipfile --deploy --system && ln -s /usr/bin/python3 /usr/bin/python

ADD checker/*.py    /checker/

EXPOSE 80

RUN python3 checker.py