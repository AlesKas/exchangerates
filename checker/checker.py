import time
import json
import requests


def telegram_bot_sendtext(bot_message):
    
    bot_token = '1477876508:AAFPWBbq2rFhthe-8oZz_i8NlOWQ8-TZeEY'
    bot_chatID = '1150615472'
    send_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + bot_chatID + '&parse_mode=Markdown&text=' + bot_message

    response = requests.get(send_text)

    return response.json()


def report(dollar_value):
    my_message = "Dollar value is: {}".format(dollar_value)
    telegram_bot_sendtext(my_message)


def check_USD_value():
    data = requests.get('https://api.exchangeratesapi.io/latest?base=USD&symbols=CZK')
    json_data = json.loads(data.text)
    
    dollar_value = json_data["rates"]["CZK"]
    dollar_without_commission = dollar_value - (dollar_value * 0.005) # Revolut takes 0.5% 
    if dollar_without_commission > 23.25:
        report(dollar_without_commission)


telegram_bot_sendtext("Starting now")
while True:
    check_USD_value()
    time.sleep(300)